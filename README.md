# Bachelor thesis

## Usage
```bash
make install  # optimized for apt-based dep systems
make build
make view
```

## Contributing
If only ...

## Authors

| [![sirfoga](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/sirfoga "Follow @sirfoga on Github") |
|---|
| [Stefano Fogarollo](https://sirfoga.github.io) |


## License
[MIT License](https://opensource.org/licenses/MIT)
