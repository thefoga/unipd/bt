cwd            := $(shell pwd)

scripts_folder := "_scripts/"

update         := sudo apt -qq update
install        := sudo apt install -y --no-install-recommends

tex_compile    := pdflatex
tex_file       := main
bib_make       := biber
gloss_make     := makeglossaries

.PHONY: all view

clean::
	rm -fv *.aux *.log *.bbl *.blg *.toc *.out *.lot *.lof
	rm -fv *.bcf *.glo *.ist *.run.xml *-blx.bib *.acn *.synctex.gz
	rm -fv */*.aux */*.log */*.bbl */*.blg */*.toc */*.out */*.lot */*.lof
	rm -fv */*.bcf */*.glo */*.ist */*.run.xml */*-blx.bib */*.acn */*.synctex.gz
	rm -fv */*/*.aux */*/*.log */*/*.bbl */*/*.blg */*/*.toc */*/*.out */*/*.lot */*/*.lof
	rm -fv */*/*.bcf */*/*.glo */*/*.ist */*/*.run.xml */*/*-blx.bib */*/*.acn */*/*.synctex.gz


cleanall::
	$(MAKE) clean
	rm -fv *.pdf

view::
	xdg-open main.pdf

install::
	$(update)                                                              && \
	$(install) texlive-full texlive-fonts-recommended texlive-latex-extra  && \
	$(install) texlive-fonts-extra dvipng texlive-latex-recommended        && \
	$(install) hunspell myspell-it myspell-en-gb                           && \
	$(install) biber

build::
	$(tex_compile) $(tex_file)
	$(gloss_make) $(tex_file)
	$(bib_make) $(tex_file)
	$(tex_compile) $(tex_file)

rebuild::
	$(MAKE) cleanall
	$(MAKE) build

debug::
	$(MAKE) rebuild
	$(MAKE) view

spell::
	cd $(scripts_folder)                                                   && \
	bash spell_check.sh
