# !/bin/bash
# coding: utf-8


# folders containing files to spell-check
FOLDERS=(
	"/ch/"
)

ROOT_FOLDER=$(dirname $PWD)  # working dir

# help functions
function spellCheckFile() {
	echo "Spell-checking $1"
	errors=( $(hunspell -d "en_GB,it_IT" -t -l $1 | tr '[:upper:]' '[:lower:]' | sort | uniq) )
	for err in ""${errors[@]}""
	do
		echo "    $err"
	done
}

for folder in ""${FOLDERS[@]}""
do
	folderPath=$ROOT_FOLDER$folder
	files=( $(find $folderPath -iname "*.tex") )  # search for .tex files
	for fil in ""${files[@]}""
	do
    	spellCheckFile $fil  # run
    done
done

exit 0


