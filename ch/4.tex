\chapter{\iecProtocol{}}
\label{cap:\iecProtocol{}}

Questo capitolo descrive il protocollo IEC-60870-5-104 e la relativa implementazione in \xmonitor{}.

\section{Il contesto}
Gli elementi principali del sistema dove \loSta{} ha implementato \iecProtocol{} sono cinque:
\begin{itemize}
  \item una lista di sensori (possibilmente che trasmettono dati con protocolli diversi);
  \item i sensori sono collegati in rete radio (IEEE 802.15.4) a un router \xmonitor{};
  \item un router \xmonitor{}, dove \loSta{} ha implementato \iecProtocol{}, usando il framework \qt{} come base per l'applicazione;
  \item il router è collegato in rete e manda attraverso TCP/IP i messaggi, secondo \iecProtocol{};
  \item la piattaforma Monet configura il router, riceve e visualizza i pacchetti dati.
\end{itemize}

\begin{figure}[tph]
  \centering
  \includegraphics[width=\linewidth]{img/overview}
  \caption{Overview del sistema}
\end{figure}

Lo stagista si è occupato del collegamento router-Monet e dell'implementazione di \iecProtocol{} nel router. Il \autoref{cap:\xmonitor{}} si occuperà di dettagliare l'architettura dell'applicazione sviluppata in \qt{}; nel \autoref{cap:ui} lo studente andrà a descrivere l'interfaccia web di gestione della configurazione.\\
Di seguito dunque una panoramica dello standard seguito.

\section{Lo standard}
Il protocollo \iecProtocol{} fa parte dello standard IEC 60870-5 che fornisce un profilo di comunicazione per mandare messaggi di \g{telecontrollo} tra due sistemi nell'ambito dell'automazione industriale di sistemi elettrici.\\
\iecProtocol{} fornisce l'accesso alla rete a IEC 60870-5-101 usando profili standard di trasporto. Nella pratica consegna messaggi \iecOne{} nello strato applicativo (layer 7) di una rete usando TCP (2404 è la porta di default). \iecProtocol{} assicura la comunicazione tra la stazione di controllo e la sotto-stazione nella rete \g{TCP/IP}. La comunicazione è basata sul \g{modello client-server}.\\\\
\g{IEC} ha definito lo standard 60870 per l'automazione di macchinari e sistemi nel campo di potenze elettriche. La parte 5 dello standard fornisce un profilo di comunicazione per lo scambio di tali messaggi. In particolare la parte 5 consta delle seguenti parti:
\begin{itemize}
  \item IEC 60870-5-1 \textit{Formati di trasmissione frame}
  \item IEC 60870-5-2 \textit{Procedure di trasmissione}
  \item IEC 60870-5-3 \textit{Struttura generale dei dati}
  \item IEC 60870-5-4 \textit{Definizione e codifica dei dati applicazione}
  \item IEC 60870-5-5 \textit{Funzioni di base dei delle applicazioni}
  \item IEC 60870-5-6 \textit{Linee guida per i test}
\end{itemize}
IEC ha anche generato standard per attività di telecontrollo, trasmissione di quantità, scambio dati e accesso alla rete:
\begin{itemize}
  \item IEC 60870-5-7 \textit{Estensione di sicurezza a} IEC 60870-5-101 \textit{e} IEC 60870-5-104
  \item IEC 60870-5-101 \textit{Attività di telecontrollo di base}
  \item IEC 60870-5-102 \textit{Trasmissione di quantità}
  \item IEC 60870-5-103 \textit{Interfaccia informativa delle attrezzature di protezione}
  \item IEC 60870-5-104 \textit{Accesso alla rete per} \iecOne{}
  \item IEC 60870-5-601 \textit{Test di controllo per} \iecOne{}
  \item IEC 60870-5-604 \textit{Test di controllo per} \iecProtocol{}
\end{itemize}

\begin{figure}[tph]
  \centering
  \includegraphics[width=0.5\linewidth]{img/iec/scada}
  \caption{Topologia di rete di un sistema di monitoraggio \g{SCADA}, \textcopyright{} \cite{site:czech_iec}}
  \label{fig:topology-scada}
\end{figure}

\subsection{Indirizzamento}
IEC 101 definisce l'indirizzamento sia nel livello di accesso alla rete che al livello dell'applicazione. L'indirizzo di un dispositivo (o \g{OA}) e l'indirizzo \g{ASDU} (o \g{CA}) sono forniti per l'identificazione della stazione finale.
\begin{enumerate}
  \item L'indirizzo del dispositivo è il numero di identificazione del dispositivo
  \item Ogni dispositivo sulla rete di comunicazione ha un indirizzo comune di ASDU. Combinando CA e OA riusciamo ad ottenere un indirizzo univoco per ogni dato elemento
\end{enumerate}

\subsection{Comunicazione}
Un concetto importante nell'indirizzamento secondo IEC 60870-5 è la differenza tra controllare e monitorare la stazione. Viene assunto per ipotesi che il sistema abbia una struttura gerarchica a controllo centralizzato: ogni stazione è o una stazione di controllo o una stazione controllata (vedi \cite{site:abb_iec}).\\
\begin{itemize}
  \item La stazione controllata viene monitorata o comandata da una stazione master (RTU). Viene anche chiamata \textit{outstation, remote station, RTU, 101-Slave} o \textit{104-Server}
  \item La stazione di controllo è una stazione in cui viene eseguito un controllo delle stazioni esterne (SCADA). In genere, è un PC con sistema SCADA; può anche essere un RTU32
\end{itemize}

IEC 101/104 definisce diverse direzioni della comunicazione:
\begin{itemize}
  \item \textit{Monitor}, dalla stazione controllata (RTU) alla stazione di controllo (PC)
  \item \textit{Control}, dalla stazione di controllo, tipica di un sistema SCADA, verso la stazione controllata, tipicamente una RTU
  \item \textit{Reversed}, quando la stazione monitorata invia comandi e la stazione di controllo sta inviando dati
\end{itemize}

\subsection{ASDU}
L'ASDU contiene 2 settori principali:
\begin{enumerate}
  \item \textit{Data unit identifier} che definisce il tipo specifico di dati e include informazioni riguardo a \g{COT}
  \item i dati stessi, cioè una lista di \g{IO} (fino ad un massimo di 127)
\end{enumerate}

\subsection{IO}
Ogni IO in un ASDU ha un univoco \g{IOA}. L'indirizzo è usato come destinazione nella direzione di controllo, e come origine nella direzione di monitor. Tutti gli IO all'interno di uno stesso ASDU devono avere lo stesso tipo: se più IO con tipo diverso devono essere trasmessi allora sono necessari più ASDU.\\
I tipi di IO sono molteplici e spaziano ogni possibile tipo di dato proveniente da un sistema che fornisce dati relativi a potenze elettriche. In particolare, nel lavoro di stage, è stato quasi sempre richiesto di mandare IO con tipi:
\begin{itemize}
  \item \textbf{9 (0x09)}, valori misurati, normalizzati (tra 0 e 1)
  \item \textbf{13 (0x0D)}, valori misurati a 16 bit (\texttt{short})
\end{itemize}

\begin{figure}[!htb]
  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[width=.9\linewidth]{img/iec/apdu}
    \caption{APDU, \textcopyright{} \cite{site:czech_iec}}
  \end{minipage}\hfill
  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[width=.9\linewidth]{img/iec/asdu}
    \caption{ASDU, \textcopyright{} \cite{site:czech_iec}}
  \end{minipage}
\end{figure}

\section{Implementazione}
\iecProtocol{} è stato prima implementato in una libreria C; in seguito definendo dei \qt{} binding a livello dell'applicazione.

\subsection{C99}
Per l'implementazione \loSta{}, su consiglio dell'azienda, si è basato sulla libreria fornita da MZ Automation GmbH, leader tedesco nello sviluppo e consulenza di protocolli di comunicazione industriale come IEC 61850, IEC 60870-5, TASE.2/ICCP e DNP3.\\
La libreria è scritta in C (versione 1999) in modo da poter essere supportata da più devices possibili. Essa è open-source e scaricabile tramite il sito ufficiale dell'azienda o da \cite{site:iec_github}.

\subsection{\qt{} binding}
A livello \qt{}, \loSta{} ha implementato un \texttt{QTimer} per eseguire il tick dello slave IEC e quindi mantenere il socket di connessione aperto.\\
Inoltre il componente \qt{} implementa il \g{design pattern} proxy in quanto modera e controlla l'accesso (semplificandolo) verso lo slave IEC: in questo modo l'applicazione chiama soltanto un subset delle funzioni di IEC, togliendo ogni complessità dal codice.

\section{Problemi rilevati}
Come succede quasi sempre in ambito embedded i problemi riscontrati sono stati molteplici ma tutti risolvibili in meno di poche ore. L'integrazione con \monet{} ha decisamente caratterizzato l'ultima parte della fase di test e collaudo del prodotto.

Monet è la piattaforma \iot{} che supporta \iecProtocol{} con cui il router deve interfacciarsi. Essa è particolarmente adatta alla visualizzazione e gestione di dati relativi a \g{smart grid} e più in generale a \g{smart city} \cite{site:siemens_monet}.

\begin{figure}[tph]
  \centering
  \includegraphics[width=0.33\linewidth]{img/logo/brands/monet}
  \caption{Logo piattaforma Monet}
\end{figure}

\subsection{Supporto del protocollo}
Uno dei problemi riscontrati è la difficoltà di Monet (o della versione di Monet fornita al cliente) di supportare i tipi di IO. In particolare \loSta{} ha notato che, seppur trasmettendo ASDU secondo il protocollo, Monet rilevava solo parte degli IO in determinati ASDU e solamente IO di tipo \textbf{9 (0x09)}, \textbf{11 (0x0B)}, \textbf{13 (0x0D)} e \textbf{15 (0x0F)}.\\
Nonostante ciò, il cliente è stato soddisfatto della trasmissione di IO in quanto i tipi richiesti erano solamente \textbf{9 (0x09)} e \textbf{11 (0x0B)}.\\\\
Nel capitolo successivo verrà descritta l'architettura (backend) dell'applicazione installata nel gateway.
